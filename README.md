# خودآموز SASS

پیش‌پردازنده‌ای است که کدهای CSS را از روی کدهای Sass که بازاستفپذیر و سلسلپذیر است براینمان می‌سازد.

## انتخابال (Selectors)

همانند css با این تفاوت که می‌توان درهم لانیدشان.

```scss
body {
    // ...
    .content {
        // ...
    }
    // ...
}
```

## متغیر

بهتره همواره اندازه‌ها در متغیرها نگهداری شوند. در SCSS همه‌ی متغیرها در فضای نام تعریفیده می‌شوند و از خط پس از تعریف دسترسپذیر هستند.

**تعریفیدن متغیر.** نحو اعلانیدن متغیر:

```scss
$text-color: #ff0000;
$text-size: 1.2rem;
```

**قرارداد.** متغیرها به شکل `$text-color` نامگذاری شوند.

**استفیدن متغیر.** نحو استفیدن از متغیر:

```scss
body {
    color: $text-color;
}

$font:'font-name';
@import url(https://fonts.google.com/css?family=#{$font})
```

## عملگرها

### عملگرهای ریاضی

<div dir="rtl" align="right" markdown="1">

* `+`: جمعیدن دو مقدار سازگار؛ نتیجه هم‌نوع عملوند سمت چپ است: <span dir="ltr" markdown="1">`1in + 1px -> 1.01042in`</span>
* `-`: کسریدن دو مقدار سازگار؛ نتیجه هم‌نوع عملوند سمت چپ است: <span dir="ltr" markdown="1">`1in - 1px -> 0.98958in`</span>
* `*`: ضربیدن دو مقدار درهم؛ بخش عددها عددها درهم و یکاها نیز درهم ضربیده می‌شوند. <span dir="ltr" markdown="1">`2rem * 4px -> 8rem*px`</span>
* `math.div(6, 2)`: تقسیمیدن آرگومان اول بر آرگومان دوم. عددها برهم و یکاها نیز برهم تقسیمیده می‌شوند.
  * **توجه.** برای استفیدن از پودمان `math` باید ابتدای فایل مورد نظر `@use "sass:math";` آورده شود.

</div>

## تابع

**تعریفیدن.**

```scss
@function sum($left, $right) {
    @return $left + $right;
}

@function strip-unit($value) {
    @return $value / ($value * 0 + 1);
}
```

**استفیدن.**

```scss
.content {
    font-size: sum(1px, 2px);
}
```

### تابع‌های درونی

<div dir="rtl" align="right" markdown="1">

* `unquote(str)`: حذفیدن `""` از رشته‌ها.
* `darken($color, $amount:[0%-100%])`: برگرداندن رنگ `$amount` درصد تیره‌تر از `$color`.
* `lighten($color, $amount:[0%-100%])`: برگرداندن رنگ `$amount` درصد روشنتر از `$color`.
* `transparentize($color, $amount:[0-1])`: برگرداندن رنگ شفافتر.
* `opacity($color, $amount:[0-1])`: برگرداندن رنگ کدرتر.

</div>

## عبارت‌های تصمیمگیری

اجراییدن دستورهایی در شرایط خاص یا تا برپاییده بودن یه شرط خاص.

### عبارت شرطی `@if`

اجراییدن یه دسته دستور تنها در شرایط خاص.

```scss
$contrast:high;

@if $contrast == high {
    color:000;
} @else if {
    color: 999;
} @else {
    color: $color-text;
}
```

### حلقه تکرار `@while`

تکراریدن مجموعه‌ای از دستورها تا برپا بودن شرط حلقه.

```scss
$j: 2;
@while $j <= 8 {
    // ...
    $j: $j + 2;
}
```

### حلقه تکرار `@for`

تکراریدن یه دسته دستور به‌ازای همه عضوهای یه مجموعه عدد.

```scss
@for $i from 1 through 6 {
    .col-#{$i} {
        width: $i * 2em;
    }
}
// OR
@for $i from 1 to 7 {
    .col-#{$i} {
        width: $i * 2em;
    }
}
```

### حلقه تکرار `@each`

تکراریدن یه دسته دستور به‌ازای همه عضوهای یه مجموعه.

```scss
$prefixes: xs, sm, md, lg, xl;

@each $prefix in $prefixes {
    .size-#{$prefix}{
        // ...
    }
}

$font-sizes: {
    tiny: 1re,
    small: 1.2rem,
    medium: 1.4rem,
    large: 2rem
}

@each $name, $size in $font-size {
    .#{$name} {
        font-size: $size;
    }
} 
```

## سلسلیدن

در Scss با تجمعیدن و ارثبردن می‌توان کلاس‌ها را سلسلید.

### تجمیعیدن (mixin)

از `@mixin` استفیده می‌شود تا بتوان قطعه کدهایی نوشته و آن‌ها را با هم تجمیعید.

**تعریفیدن.**

```scss
@mixin warning {
    color: $text-color;
    background-color: $bg-color;
}

// MIXIN with parameter
@mixin rounded($radius) {
    border-radius: $radius;
}

@mixin text($size, $weight:normal) {
    font: {
        size: $size;
        weight: $weight;
    }
}

@mixin box-shadow($shadows...) {
    box-shadow: $shadows;
}

// MIXIN with content
@mixin mobile-style {
    body .m {
        @content;
    }
}
```

**استفیدن.**

```scss
body {
    @include warning;
    @include rounded(1rem);
    @include text($size:1.2rem);
    @include box-shadow(2px 0px 4px #999, 1px 1px 6px #secondary-color)
}

@include mobile-style {
    body {
        //...
    }
}
```

**توجه.** پیش‌پردازنده‌ی Sass هنگام برخورد با عبارت `@include warning;` تنها محتوای میکسین `warning` را می‌جایگزیند.

### ارثبردن (Inheritance)

با استفیدن از امریه (directive) `@extend` می‌توان یه کلاس را گسترد.

```scss
.error {
    // error styles
}

.critical-error {
    @extend .error;
    // critical-error styles
}
```

**توجه.** درواقع پیشپردازنده Sass برای ایجاد رابطه‌ی ارثبری انتخابال زیرکلاس را به زبرکلاس می‌افزاید. کد بالا بشکل زیر همگردانده می‌شود:

```css
.error,
.critical-error {
    /* error styles */
}

.critical-error {
    /* critical-error styles */
}
```

**نکته.** امکان ارثبری چندگانه نیز وجود دارد.

```scss
.error {
    // error styles
}
.button {
    // button styles
}
.button-error {
    @extend .error;
    @extend .button;
}
```

**نکته.** زنجیریدن ارثبری هم ممکن است.

```scss
.error {
    // error styles
}
.button {
    // button styles
}
.button-error {
    @extend .error;
    @extend .button;
}
.button-error-sub {
    @extend .button-error;
    // button-error-sub styles
}
```

**نکته.** درون پرسجوی رسانه نمی‌توان از کلاس‌های بیرونش ارثبرد.

```scss
// THIS NOT WORK!
.error {
    // error styles
}
@media screen {
    .button-error {
        @extend .error;
        // button styles
    }
}
```

**کلیدواژه `optional`.** هنگامیکه بخواهیم از یه کلاس درصورت وجود داشتن ارثبریم، از این کلیدواژه بشکل زیر می‌استفیم.

```scss
.button-error {
    @extend .error !optional;
}
```

#### تعریفیدن کلاس انتزاعی

کلاسی که هیچ انتخابالی نداشته و تنها برای ارثبری تعریفیده می‌شود.

```scss
%highlight {
    // highlight styles
}

.sub-title {
    @extend %highlight;
    // sub-title styles
}
```

## پرسجویدن رسانه (media query)

همانند css با این تفاوت که می‌توان درون انتخابنده‌ها نیز لانیدشان.

```scss
.content {
    // ...

    @media only screen and (max-width: 960px) {
        // ...
    }

    // ...
}
```

## تکیدن (partial)

می‌توانیم کدهایمان را برای سادیدن مدیریت در چندین فایل جدا بنویسیم.

**توجه.** تکه‌ها را با پیشوند `_` می‌نامیم تا توسط پیش‌پردازنده‌ی Sass هنگردانده نشود.

**وروداندن .** تنها کافیه نام تکه، بدون پیشوند `_` و پسوند `scss`، نوشته شود.

```scss
// _variables.scss
@import "/path/to/partial";
```

**توجه.** پیش‌پردازنده‌ی Sass درواقع کد `@import "/path/to/partial";` را با محتوای فایل `_variables.scss` می‌جایگزیند. یعنی، اگر این عبارت در دو مرتبه ورودانده شود، محتوای `_variables.scss` دو بار در فایل CSS خروجی همگردانده می‌شود.
